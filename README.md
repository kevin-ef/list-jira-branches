## Display jira summary for git branches

Will call jira to get the summary of all git branches that have
a Jira ticket in their name e.g. feat/EFOM-2001 so it just needs to follow
the format `[feat | bug | whatever]/[Jira ID]`

To start first create a file called `auth-token.js` and then contents
should look like this:

```js
module.exports = {
  authUser: "", // Jira username
  authToken: "" // Jira api token (https://id.atlassian.com/manage-profile/security/api-tokens)
};
```

Once you have created this file run the command `npm i` and then run `npm install -g .`
and everything should be setup.

Then on any project run the following command `list-jira-branches` and it should
give you something like:

```
feat/EFOM-1048 - [ETA Aug 17] CV parsing for TRT application form - [In Progress]
staging
feat/ECGDM-218
feat/EFOM-1037 - [ETA Aug 6] EFKT CN - Baidu Leads API - [Done]
feat/EFOM-940 - [ETA Jul 23] Set up Applicant API for TRT Storyblok site - [Done]
storyblok-api
master
EFOM-940
feat/OMS-4925 - [ETA Jun 1] Upload leads in Xiaoetong to salesforce automatically - [Done]
feat/EFOM-797 - [ETA May 21] ODB Component for EFEC - [Done]
```
