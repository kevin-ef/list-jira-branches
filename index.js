#!/usr/bin/env node

const { exec } = require("child_process");
const chalk = require("chalk");
const axios = require("axios");
const { authUser, authToken } = require("./auth-token");

const getAuthHeader = () => {
  const data = `${authUser}:${authToken}`;
  const buff = new Buffer.from(data);
  const base64data = buff.toString("base64");
  return `Basic ${base64data}`;
};

const listBranches = async () => {
  return new Promise((resolve, reject) =>
    exec(
      `git for-each-ref --count=10 --sort=-committerdate refs/heads/ --format="%(refname:short)"`,
      (error, stdout, stderr) => {
        if (error) {
          reject(`error: ${error.message}`);
          return;
        }
        if (stderr) {
          reject(`stderr: ${stderr}`);
          return;
        }
        resolve(stdout.split("\n"));
      }
    )
  );
};

const getTicketSummary = async ticket => {
  try {
    const { data } = await axios.get(
      `https://jirae1.atlassian.net/rest/api/3/issue/${ticket}?fields=summary,status`,
      {
        headers: {
          Authorization: getAuthHeader(),
          Accept: "application/json"
        }
      }
    );

    const status = data.fields.status ? data.fields.status.name : "";

    return { summary: data.fields.summary, status };
  } catch (e) {
    return { summary: "", status: "" };
  }
};

const run = async () => {
  const branches = await listBranches();
  const ticketBranches = branches.map(branch => {
    const [, ticket] = branch.split("/");
    if (ticket && ticket.match(/[A-Z]+-[0-9]+/g)) {
      return { ticket, branch };
    }
    return { branch };
  });

  const list = await Promise.all(
    ticketBranches.map(async ({ ticket, branch }) => {
      if (!ticket) {
        return { branch, summary: "", status: "" };
      }
      const { summary, status } = await getTicketSummary(ticket);

      return { summary, status, branch };
    })
  );

  list.forEach(({ summary, branch, status }) => {
    if (!summary) {
      console.log(chalk.white.bold(branch));
      return;
    }

    console.log(
      `${chalk.white.bold(branch)} - ${chalk.yellow.bold(
        summary
      )} - [${status}]`
    );
  });
};

run();
